//  Created by sunmumu

#import <UIKit/UIKit.h>
#import "GDPTextViewConstant.h"

@interface GDPTextView : UIView

@property (nonatomic, strong) UITextView                        *textView;
@property (nonatomic, assign)   BOOL                            hiddenNumberTip;

- (instancetype)initWithFrame:(CGRect)frame placeholder:(NSString *)placeholder limitNumber:(NSInteger)limitNumber textBlock:(StringBlock)textBlock;

@end
