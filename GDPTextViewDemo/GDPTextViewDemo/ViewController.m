//
//  ViewController.m
//  GDPTextViewDemo
//
//  Created by sun on 2022/8/2.
//

#import "ViewController.h"
#import "GDPTextView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI {
    GDPTextView *textView = [[GDPTextView alloc] initWithFrame:CGRectMake(20, 100, [UIScreen mainScreen].bounds.size.width - 40, 180) placeholder:@"请输入内容" limitNumber:100 textBlock:^(NSString *result) {
        
    }];
    textView.backgroundColor = [UIColor blackColor];
//    textView.textView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:textView];
}

@end
