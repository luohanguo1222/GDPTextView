Pod::Spec.new do |s|
  s.name         = 'GDPTextView'
  s.version      = '0.0.6'
  s.summary      = 'TextView有占位符合字数限制和提示'
  s.homepage     = 'https://gitee.com/luohanguo1222/GDPTextView'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'luohanguo' => 'wy335089101@163.com' }
  s.platform     = :ios
  s.ios.deployment_target = '8.0'
  s.source       = { :git => 'https://gitee.com/luohanguo1222/GDPTextView.git', :tag => s.version.to_s }
  s.requires_arc = true
  s.source_files = 'GDPTextView/**/*.{h,m}'
  s.public_header_files = 'GDPTextView/**/*.{h}'
  s.frameworks = ['UIKit', 'Foundation']
  s.dependency 'Masonry'
end