# GDPTextView

#### 介绍
TextView有占位符合字数限制和提示

## Use
法一 cocopods引用
pod 'GDPTextView', :git => 'https://gitee.com/luohanguo1222/GDPTextView.git'


法二 文件拖入项目中
1. 把GDPTextView文件夹拖入项目中
2. 在控制器中#import "GDPTextView.h"

## License

GDPTimeDownButton is released under a MIT License. See LICENSE file for details.